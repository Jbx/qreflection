#pragma once

// Qt
#include <QJsonObject>
#include <QJsonArray>

// STL
#include <variant>

template<typename T>
using RefWrap = std::reference_wrapper<T>;

using Value = std::variant<
    RefWrap<bool>,
    RefWrap<int>,
    RefWrap<double>,
    RefWrap<QString>,
    RefWrap<QTime>,
    RefWrap<QDateTime>>;

template <typename T>
concept ValueType =
    std::integral_constant<bool,
                           std::is_same<T, bool>::value ||
                               std::is_same<T, int>::value ||
                               std::is_same<T, double>::value ||
                               std::is_same<T, QString>::value ||
                               std::is_same<T, QTime>::value ||
                               std::is_same<T, QDateTime>::value
                           >::value;

struct ValueAttribute
{
    QString name;
    Value value;
};

struct ArrayAttribute;
struct StructAttribute;

using Attribute = std::variant<
    ValueAttribute,
    ArrayAttribute,
    StructAttribute>;

using AttributeVector = std::vector<Attribute>;

template <typename T>
QString getTypeName();

template <typename T>
AttributeVector getAttributes(T&);

template <typename T>
AttributeVector getAttributes(const T& t)
{
    return getAttributes(const_cast<T&>(t));
}

#define ATTRIBUTE(NAME) attr(#NAME, data.NAME)

#define REGISTER_TYPE(TYPE, ...) \
template <> \
inline QString getTypeName<TYPE>() \
{ \
    static const QString typeName = #TYPE; \
    return typeName; \
} \
template <> \
inline AttributeVector getAttributes(TYPE& data) \
{ \
    return {__VA_ARGS__}; \
} \

struct ValueArray
{
    std::function<size_t()> count;
    std::function<Value(size_t index)> get;    
    std::function<Value()> emplace;
    std::function<void()> clear;
};

struct StructArray
{
    std::function<size_t()> count;
    std::function<AttributeVector(size_t index)> get;
    std::function<AttributeVector()> emplace;
    std::function<void()> clear;
};

struct ArrayAttribute
{
    QString name;
    std::variant<
        ValueArray,
        StructArray> array;
};

struct StructAttribute
{
    QString name;
    AttributeVector attributes;
};

template <typename T>
concept StructType = !ValueType<T>;

template <ValueType T>
inline ValueAttribute attr(QString&& name, T& value)
{
    return ValueAttribute{.name = name, .value = value};
}

template <ValueType T>
inline ArrayAttribute attr(QString&& name, std::vector<T>& values)
{
    ValueArray array;

    array.get = [&values](size_t index) {
        return std::ref(values[index]);
    };

    array.count = [&values]() {
        return values.size();
    };

    array.emplace = [&values]() {
        auto& value = values.emplace_back(T{});
        return std::ref(value);
    };

    array.clear = [&values]() {
        values.clear();
    };

    return ArrayAttribute{
        .name = name,
        .array = array,
    };
}

template <StructType T>
inline ArrayAttribute attr(QString&& name, std::vector<T>& values)
{
    StructArray array;

    array.get = [&values](size_t index) {
        return getAttributes(values.at(index));
    };

    array.count = [&values]() {
        return values.size();
    };

    array.emplace = [&values]() {
        auto& value = values.emplace_back(T{});
        return getAttributes(value);
    };

    array.clear = [&values]() {
        values.clear();
    };

    return ArrayAttribute{
        .name = name,
        .array = array,
    };
}

template <StructType T>
inline StructAttribute attr(QString&& name, T& value)
{
    return StructAttribute{.name = name, .attributes = getAttributes(value)};
}

// Overload pattern, for explanation see: https://www.cppstories.com/2018/09/visit-variants/
template<class... Ts> struct overload : Ts... { using Ts::operator()...; };
// Next line not needed in C++20, waiting for full emscripten support...
template<class... Ts> overload(Ts...) -> overload<Ts...>;

// Json

inline QJsonObject toJson(const std::vector<Attribute>& attributes);

template <typename T>
QJsonObject toJson(const T& t)
{
    auto attributes = getAttributes<T>(t);
    return toJson(attributes);
}

inline void fromJson(
    const QJsonObject& json,
    std::vector<Attribute>& attributes);

template <typename T>
T fromJson(const QJsonObject& json)
{
    T t;
    auto attributes = getAttributes(t);
    fromJson(json, attributes);
    return t;
}

inline QVariantMap toVariantMap(
    const std::vector<Attribute>& attributes);

template <typename T>
QVariantMap toVariant(const T& t)
{
    return toVariantMap(getAttributes(t));
}

void fromVariantMap(
    const QVariantMap& json,
    std::vector<Attribute>& attributes);

template <typename T>
T fromVariant(const QVariantMap& map)
{
    T t;
    auto attributes = getAttributes(t);
    fromVariantMap(map, attributes);
    return t;
}

template <typename T>
QJsonArray toJsonArray(const std::vector<T>& array)
{
    QJsonArray result;
    std::transform(
        array.begin(),
        array.end(),
        std::back_inserter(result),
        [](const auto& value) { return value; });
    return result;
}

template <typename T>
std::vector<T> fromJsonArray(const QJsonArray& array)
{
    std::vector<T> result;
    std::transform(
        array.begin(),
        array.end(),
        std::back_inserter(result),
        [](const auto& value) { return value.toVariant().template value<T>(); });
    return result;
}

inline QJsonValue toJsonValue(
    const Value& value)
{
    QJsonValue jsonValue;
    std::visit(
        overload{
                 [&jsonValue](bool value) { jsonValue = value; },
                 [&jsonValue](int value) { jsonValue = value; },
                 [&jsonValue](double value) { jsonValue = value; },
                 [&jsonValue](const QString& value) { jsonValue = value; },
                 [&jsonValue](const QTime& value) { jsonValue = value.toString(); },
                 [&jsonValue](const QDateTime& value) { jsonValue = value.toString(); },
                 }, value);
    return jsonValue;
}

inline QJsonValue toJsonValue(
    const ValueAttribute& attribute)
{
    return toJsonValue(attribute.value);
}

inline QJsonValue toJsonValue(
    const ArrayAttribute& attribute)
{
    QJsonArray jsonArray;
    std::visit(
        overload{
            [&jsonArray](const ValueArray& array) {
                for (int i = 0; i < array.count(); ++i)
                {
                    jsonArray.push_back(toJsonValue(array.get(i)));
                }
            },
            [&jsonArray](const StructArray& array) {
                for (int i = 0; i < array.count(); ++i)
                {
                    jsonArray.push_back(toJson(array.get(i)));
                }
            },
        }, attribute.array);

    return jsonArray;
}

inline QJsonValue toJsonValue(
    const StructAttribute& attribute)
{
    return toJson(attribute.attributes);
}

inline QJsonObject toJson(const std::vector<Attribute>& attributes)
{
    QJsonObject json;
    for (const auto& attribute : attributes)
    {
        std::visit(
            overload{
                     [&json](const ValueAttribute& attribute) { json[attribute.name] = toJsonValue(attribute); },
                     [&json](const ArrayAttribute& attribute) { json[attribute.name] = toJsonValue(attribute); },
                     [&json](const StructAttribute& attribute) { json[attribute.name] = toJsonValue(attribute); },
                     }, attribute);
    }
    return json;
}

inline void fromJsonValue(
    const QJsonValue& jsonValue,
    Value& value)
{
    std::visit(
        overload{
                 [&jsonValue](bool& value) { value = jsonValue.toBool(); },
                 [&jsonValue](int& value) { value = jsonValue.toInt(); },
                 [&jsonValue](double& value) { value = jsonValue.toDouble(); },
                 [&jsonValue](QString& value) { value = jsonValue.toString(); },
                 [&jsonValue](QTime& value) { value = QTime::fromString(jsonValue.toString()); },
                 [&jsonValue](QDateTime& value) { value = QDateTime::fromString(jsonValue.toString()); },
                 }, value);
}

inline void fromJsonValue(
    const QJsonValue& jsonValue,
    ValueAttribute& attribute)
{
    fromJsonValue(jsonValue, attribute.value);
}

inline void fromJsonValue(
    const QJsonValue& jsonValue,
    ArrayAttribute& attribute)
{
    auto jsonArray = jsonValue.toArray();

    std::visit(
        overload{
            [&jsonArray](ValueArray& array) {
                array.clear();
                for (const auto& jsonValue : jsonArray)
                {
                    auto value = array.emplace();
                    fromJsonValue(jsonValue, value);
                }
            },
            [&jsonArray](StructArray& array) {
                array.clear();
                for (const auto& jsonValue : jsonArray)
                {
                    auto value = array.emplace();
                    fromJson(jsonValue.toObject(), value);
                }
            },
        }, attribute.array);
}

inline void fromJsonValue(
    const QJsonValue& jsonValue,
    StructAttribute& attribute)
{
    fromJson(jsonValue.toObject(), attribute.attributes);
}

inline QString getName(const Attribute& attribute)
{
    QString name;
    std::visit(
        overload{
                 [&name](const ValueAttribute& attribute) { name = attribute.name; },
                 [&name](const ArrayAttribute& attribute) { name = attribute.name; },
                 [&name](const StructAttribute& attribute) { name = attribute.name; },
                 }, attribute);
    return name;
}

inline void fromJson(const QJsonObject& json, std::vector<Attribute>& attributes)
{
    for (auto it = json.begin(); it != json.end(); ++it)
    {
        const auto& jsonKey = it.key();
        const auto& jsonValue = it.value();

        auto attributeIt = std::find_if(
            attributes.begin(),
            attributes.end(),
            [&jsonKey](const auto& attribute) { return getName(attribute) == jsonKey; });

        if (attributeIt != attributes.end())
        {
            std::visit(
                overload{
                         [&jsonValue](ValueAttribute& attribute) { fromJsonValue(jsonValue, attribute); },
                         [&jsonValue](ArrayAttribute& attribute) { fromJsonValue(jsonValue, attribute); },
                         [&jsonValue](StructAttribute& attribute) { fromJsonValue(jsonValue, attribute); },
                         }, *attributeIt);
        }
        else
        {
            qWarning() << "Unknown field" << jsonKey;
        }
    }
}

template <typename T>
QVariantList toVariantList(const std::vector<T>& array)
{
    QVariantList result;
    std::transform(
        array.begin(),
        array.end(),
        std::back_inserter(result),
        [](const auto& value) {
            return value;
        });
    return result;
}

template <typename T>
std::vector<T> fromVariantList(const QVariantList& list)
{
    std::vector<T> result;
    std::transform(
        list.begin(),
        list.end(),
        std::back_inserter(result),
        [](const auto& value) {
            return value.template value<T>();
        });
    return result;
}

inline QVariant toVariant(const Value& value)
{
    QVariant variant;
    std::visit(
        overload{
                 [&variant](bool value) { variant = value; },
                 [&variant](int value) { variant = value; },
                 [&variant](double value) { variant = value; },
                 [&variant](const QString& value) { variant = value; },
                 [&variant](const QTime& value) { variant = value; },
                 [&variant](const QDateTime& value) { variant = value; },
                 }, value);
    return variant;
}

inline void fromVariant(const QVariant& variant, Value& value)
{
    std::visit(
        overload{
                 [&variant](bool& value) { value = variant.toBool(); },
                 [&variant](int& value) { value = variant.toInt(); },
                 [&variant](double& value) { value = variant.toDouble(); },
                 [&variant](QString& value) { value = variant.toString(); },
                 [&variant](QTime& value) { value = variant.toTime(); },
                 [&variant](QDateTime& value) { value = variant.toDateTime(); },
                 }, value);
}

inline QVariantMap toVariantMap(const std::vector<Attribute>& attributes);
inline void fromVariantMap(const QVariantMap& map, std::vector<Attribute>& attributes);

inline QVariantList toVariantList(
    const ArrayAttribute& attribute)
{
    QVariantList list;
    std::visit(
        overload{
            [&list](const ValueArray& array) {
                for (int i = 0; i < array.count(); ++i)
                {
                    list.push_back(toVariant(array.get(i)));
                }
            },
            [&list](const StructArray& array) {
                for (int i = 0; i < array.count(); ++i)
                {
                    list.push_back(toVariantMap(array.get(i)));
                }
            },
        }, attribute.array);
    return list;
}

inline void fromVariantList(
    const QVariantList& list,
    ArrayAttribute& attribute)
{
    std::visit(
        overload{
            [&list](ValueArray& array) {
                array.clear();
                for (const auto& variant : list)
                {
                    auto value = array.emplace();
                    fromVariant(variant, value);
                }
            },
            [&list](StructArray& array) {
                array.clear();
                for (const auto& variant : list)
                {
                    auto value = array.emplace();
                    fromVariantMap(variant.toMap(), value);
                }
            },
        }, attribute.array);
}

inline QVariantMap toVariantMap(const std::vector<Attribute>& attributes)
{
    QVariantMap map;

    for (const auto& attribute : attributes)
    {
        std::visit(
            overload{
                     [&map](const ValueAttribute& attribute) { map[attribute.name] = toVariant(attribute.value); },
                     [&map](const ArrayAttribute& attribute) { map[attribute.name] = toVariantList(attribute); },
                     [&map](const StructAttribute& attribute) { map[attribute.name] = toVariantMap(attribute.attributes); },
                     }, attribute);
    }
    return map;
}

inline void fromVariantMap(const QVariantMap& map, std::vector<Attribute>& attributes)
{
    for (auto it = map.begin(); it != map.end(); ++it)
    {
        const auto& mapKey = it.key();
        const auto& mapValue = it.value();

        auto attributeIt = std::find_if(
            attributes.begin(),
            attributes.end(),
            [&mapKey](const auto& attribute) { return getName(attribute) == mapKey; });

        if (attributeIt != attributes.end())
        {
            std::visit(
                overload{
                         [&mapValue](ValueAttribute& attribute) { fromVariant(mapValue, attribute.value); },
                         [&mapValue](ArrayAttribute& attribute) { fromVariantList(mapValue.toList(), attribute); },
                         [&mapValue](StructAttribute& attribute) { fromVariantMap(mapValue.toMap(), attribute.attributes); },
                         }, *attributeIt);
        }
        else
        {
            qWarning() << "Unknown field" << mapKey;
        }
    }
}


