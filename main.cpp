
#include <QJsonArray>

#include "reflection.h"

struct Credential
{
    QString login;
    QString password;
};

REGISTER_TYPE(
    Credential,
    ATTRIBUTE(login),
    ATTRIBUTE(password),
    );

struct Post
{
    QString content;
    QDateTime date;
};

REGISTER_TYPE(
    Post,
    ATTRIBUTE(content),
    ATTRIBUTE(date),
    );

struct User
{
    bool admin = false;
    int age = 0;
    QString firstName;
    QString lastName;
    QString email;
    Credential credential;
    std::vector<Post> posts;
    std::vector<int> friends;
};

REGISTER_TYPE(
    User,
    ATTRIBUTE(admin),
    ATTRIBUTE(age),
    ATTRIBUTE(firstName),
    ATTRIBUTE(lastName),
    ATTRIBUTE(email),
    ATTRIBUTE(credential),
    ATTRIBUTE(posts),
    ATTRIBUTE(friends),
    );

int main(int argc, char *argv[])
{
    User user;

    user.credential.login = "test";
    user.posts = {
        {"I love apples", QDateTime::currentDateTime()},
        {"My name is Michel", QDateTime::currentDateTime()}
    };

    user.friends = {12, 24, 32};

    auto json = toJson(user);
    qDebug() << json;

    auto user2 = fromJson<User>(json);
    //user2.credential.password = "secret";
    auto json2 = toJson(user2);
    qDebug() << json2;
    qDebug() <<  (json == json2);

    auto variant = toVariant(user2);
    qDebug() << variant;

    auto user3 = fromVariant<User>(variant);
    //user3.credential.password = "1234";
    auto variant2 = toVariant(user3);
    qDebug() << variant2;
    qDebug() <<  (variant == variant2);
}
